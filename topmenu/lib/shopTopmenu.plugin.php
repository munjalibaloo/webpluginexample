<?php
class shopTopmenuPlugin extends shopPlugin 
{

    static public function getPopBrands($koren_id = 0, $lim = 15) {
        
        $pathAll = wa()->getDataUrl("categories/", true, 'shop');
        $path2 = wa()->getDataUrl("categories/optimgs/", true, 'shop');
        $path2_exists = wa()->getDataPath("categories/optimgs/", true, 'shop');

        $mTopmenuBrands = new shopTopmenuBrandsModel();
        // $aBrands = $mTopmenuBrands->getByField("koren_id", $koren_id, true);
        $aBrands = $mTopmenuBrands->query("SELECT * FROM shop_topmenu_brands WHERE koren_id = '{$koren_id}' LIMIT {$lim}")->fetchAll();
        if ($aBrands) {
            $cat_ids = array();
            foreach ($aBrands as $oBrand) {
                $cat_ids[] = $oBrand['brand_id'];
            }
            if ($cat_ids) {
                $cat_str = implode(",", $cat_ids);
                $aImgs = $mTopmenuBrands->query("SELECT id, category_id, ext FROM shop_catimg WHERE category_id IN ({$cat_str})")->fetchAll("category_id");
                if ($aImgs) {
                    foreach ($aBrands as &$oBrand) {
                        if (isset($aImgs[$oBrand['brand_id']])) {
                            $oImg = $aImgs[$oBrand['brand_id']];
                            
                            if (file_exists($path2_exists.$oImg['id'].".".$oImg['ext'])) {
                                $oBrand['img_full'] = $path2.$oImg['id'] . "." . $oImg['ext'];
                            } else {
                                $oBrand['img_full'] = $pathAll.$oBrand['brand_id']."/".$oImg['id'].".".$oImg['ext']; 
                            }
                        }
                    }
                    unset($oBrand);
                }
            }
            
            return $aBrands;
        }
    }
    static public function generateTopMenu() {
        $mTopmenu = new shopTopmenuModel();
        $aGrps = $mTopmenu->query("SELECT * FROM shop_topmenu WHERE parent_id = 0 ORDER BY sort_order, name")->fetchAll();
        if ($aGrps) {
            foreach ($aGrps as $ic => $oGrp) {
                $aSubs = $mTopmenu->query("SELECT * FROM shop_topmenu WHERE parent_id = ".$oGrp['id']." ORDER BY level, sort_order, name")->fetchAll();
                if ($aSubs) {
                    foreach ($aSubs as $oSub) {
                        $aGrps[$ic]['level_'.$oSub['level']][] = $oSub;
                    }
                }
            }
        }
        return $aGrps;
    }
    static public function generateTopMenuTree($hd = false) {
        
        $mTopmenu = new shopTopmenuTreeModel();
        $result = array();
        $aGrps = $mTopmenu->query("SELECT id, name, url, level, `hide` FROM shop_topmenu_tree WHERE parent_id = 0 && hide = 0 ORDER BY `sort_order`, name")->fetchAll();
        if ($aGrps) {
            foreach ($aGrps as $oGrp) {
                $result[$oGrp['id']] = $oGrp;
                $aSubGrps = $mTopmenu->query("SELECT id, name, url, level, `hide` FROM shop_topmenu_tree WHERE parent_id = ".$oGrp['id']." && level = 1 && hide = 0 ORDER BY `sort_order`, name")->fetchAll();
                if ($aSubGrps) {
                    foreach ($aSubGrps as &$oSubGrp) {
                        $aSubSub = $mTopmenu->query("SELECT id, name, url, level, `hide`, pop FROM shop_topmenu_tree WHERE parent_id = ".$oSubGrp['id']."  && level = 2 && hide = 0 ORDER BY `sort_order`, name")->fetchAll();
                        if ($aSubSub) {
                            foreach ($aSubSub as $oSubSub) {
                                $oSubGrp['tree'][] = $oSubSub;
                            }
                        }
                        $result[$oGrp['id']]['tree'][$oSubGrp['id']] = $oSubGrp;
                    }
                    unset($oSubGrp);
                }
                $aSubBrands = $mTopmenu->query("SELECT id, name, url, level, `hide`, img FROM shop_topmenu_tree WHERE parent_id = ".$oGrp['id']." && level = 4 && `hide` = 0 ORDER BY RAND ()")->fetchAll();
                if ($aSubBrands) {
                    foreach ($aSubBrands as $oSubBrand) {
                        $result[$oGrp['id']]['brands'][$oSubBrand['id']] = $oSubBrand;
                    }
                    if ($hd) {
                        
                        if (isset($result[$oGrp['id']]['brands'])) {
                            $ic = 0;
                            foreach ($result[$oGrp['id']]['brands'] as $brand_id => $brand) {
                                if ($brand['hide'] == 0) {
                                    $ic++;
                                }
                                if ($brand['hide'] == 1 || $ic >= $hd) {
                                    $result[$oGrp['id']]['brands_hd'][$brand_id] = $brand;
                                    unset($result[$oGrp['id']]['brands'][$brand_id]);    
                                }
                                
                            }
                        }

                        
                    }
                }

            }
        }
        return $result;
    }
}