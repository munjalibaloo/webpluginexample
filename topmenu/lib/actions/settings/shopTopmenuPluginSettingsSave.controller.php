<?php

class shopTopmenuPluginSettingsSaveController extends waJsonController {

    public function execute() {

        $oper_type = waRequest::post('oper_type');

        switch ($oper_type) {
            case "save_level":
                $topmenu = waRequest::post('topmenu');
                if (isset($topmenu['items']) && isset($topmenu['parent_id']) && isset($topmenu['level'])) {
                    $parent_id = $topmenu['parent_id'];
                    $level = $topmenu['level'];
                    if ($level > 0) {
                        if ($parent_id > 0) {
                            $mTopMenu = new shopTopmenuModel();
                            $oParent = $mTopMenu->getById($parent_id);
                            if ($oParent) {
                                foreach ($topmenu['items'] as $item_id => $item) {
                                    if ($item_id > 0) {
                                        $oItem = $mTopMenu->getById($item_id);
                                        if ($oItem) {
                                            if (isset($item['name']) && $item['name'] != "" && isset($item['url']) && $item['url'] != "") {
                                                $upd_arr = array();
                                                $upd_arr["name"] = htmlspecialchars(strip_tags(trim($item['name'])));
                                                $upd_arr["url"] = htmlspecialchars(strip_tags(trim($item['url'])));
                                                
                                                if (isset($item['sort_order']) && $item['sort_order'] != $oItem['sort_order']) {
                                                    $upd_arr["sort_order"] = intval($item['sort_order']);
                                                }
                                                if ($parent_id != $oItem['parent_id']) {
                                                    $upd_arr["parent_id"] = intval($parent_id);   
                                                }
                                                if ($level != $oItem['level']) {
                                                    $upd_arr["level"] = intval($level);   
                                                }
                                               
                                                $mTopMenu->updateById($item_id, $upd_arr);

                                                $this->response['item']['name'] = $upd_arr['name'];
                                                $this->response['item']['url'] = $upd_arr['url'];
                                                $this->response['item']['id'] = $item_id;
                                                $this->response['oper'] = "save";
                                                
                                            } else {
                                                $this->errors = "Ошибка: Для изменеия пункта меню обязательно должно быть название и ссылка";
                                            }
                                        } else {
                                            $this->errors = "Ошибка: неизвестный пункт меню для изменения";
                                        }
                                    } else {
                                        $ins_arr = array();
                                        if (isset($item['name']) && $item['name'] != "" && isset($item['url']) && $item['url'] != "") {
                                            $ins_arr["name"] = htmlspecialchars(strip_tags(trim($item['name'])));
                                            $ins_arr["url"] = htmlspecialchars(strip_tags(trim($item['url'])));
                                            if (isset($item['sort_order']) && $item['sort_order'] >0) {
                                                $ins_arr["sort_order"] = intval($item['sort_order']);
                                            }
                                            $ins_arr["parent_id"] = intval($parent_id);   
                                            $ins_arr["level"] = intval($level);   
                                            $ins_arr["active"] = 1;
                                            $menu_id = $mTopMenu->insert($ins_arr);
                                            if ($menu_id > 0) {
                                                $this->response['oper'] = "add";
                                                $path = wa()->getAppPath('plugins/topmenu', 'shop');
                                                $oItem = $mTopMenu->getById($menu_id);
                                                $view = wa()->getView();
                                                $view->assign("oItem", $oItem);
                                                $this->response['item']['name'] = $ins_arr['name'];
                                                $this->response['item']['url'] = $ins_arr['url'];
                                                $this->response['item']['id'] = $menu_id;
                                                $this->response['item']['html'] = $view->fetch($path.'/templates/itemMenu.html');
                                                
                                            } else {
                                                $this->errors = "Ошибка: Не удалось создать пункт меню";
                                            }
                                            

                                        } else {
                                            $this->errors = "Ошибка: Для создания пункта меню обязательно должно быть название и ссылка";
                                        }
                                    }
                                }
                            } else {
                                $this->errors = "Ошибка: Неизвестный корневой пункт меню";
                            }
                        } else {
                            $this->errors = "Ошибка: для уровня ".$level." обязательно должен быть родительский пункт меню";
                        }
                    }
                } else {
                    $this->errors = "Ошибка: нет возможности изменить пункт меню.";
                }
            break;
            case "delete_item":
                $item_id = waRequest::post("sub_id");
                if ($item_id > 0) {
                    $mTopMenu = new shopTopmenuModel();
                    $oItem = $mTopMenu->getById($item_id);
                    if ($oItem) {
                        $mTopMenu->deleteById($item_id);
                        $this->response['ok'] = 1;
                    } else {
                        $this->errors = "Ошибка: не существует такого пункта меню";
                    }
                } else {
                    $this->errors = "Ошибка: утерян идентификатор пункта меню";
                }
            break;
            case "del_menu":
                $id = waRequest::post("id", 0, "int");
                if ($id > 0) {
                    $mTopmenu = new shopTopmenuTreeModel();
                    $oTopmenu = $mTopmenu->getById($id);
                    if ($oTopmenu) {
                        if ($oTopmenu['level'] > 1) {
                            $mTopmenu->deleteById($id);
                        } elseif ($oTopmenu['level'] == 1) {
                            $aSubmenus = $mTopmenu->getByField("parent_id", $oTopmenu['id'], true);
                            if ($aSubmenus) {
                                foreach ($aSubmenus as $oSubmenu) {
                                    $mTopmenu->deleteById($oSubmenu['id']);
                                }
                            }
                            $mTopmenu->deleteById($id);
                            
                        }
                        $this->response['tp'] = $oTopmenu['level'];
                    } else {
                        $this->errors='Ошибка: Неверный идентификатор пункта меню';    
                    }

                } else {
                    $this->errors='Ошибка: Утерян идентификатор пункта меню';
                }
            break;
            case "set_act":
                $id = waRequest::post("id", 0, "int");
                if ($id > 0) {
                    $mTopmenu = new shopTopmenuTreeModel();
                    $oTopmenu = $mTopmenu->getById($id);
                    if ($oTopmenu) {
                        $act = waRequest::post("act", 0, "int");
                        $act = abs(1 - $act);
                        $mTopmenu->updateById($oTopmenu['id'], array("hide" => $act));
                        $this->response['ok'] = 1;
                    } else {
                        $this->errors = 'Ошибка: Неверный идентификатор пункта меню';    
                    }
                } else {
                    $this->errors = 'Ошибка: Утерян идентификатор пункта меню';
                }
            break;
            case "new_save":
                $topmenu = waRequest::post("topmenu", array());
                if ($topmenu && count($topmenu) > 0) {
                    if (isset($topmenu['id']) && $topmenu['id'] > 0) {
                        $id = $topmenu['id'];
                        unset($topmenu['id']);
                        $mTopmenu = new shopTopmenuTreeModel();
                        $oTopmenu = $mTopmenu->getById($id);
                        if ($oTopmenu) {
                            if (isset($topmenu['hide'])) {
                                $topmenu['hide'] = 1;
                            } else {
                                $topmenu['hide'] = 0;
                            }
                            $mTopmenu->updateById($id, $topmenu);
                            $topmenu['level'] = $oTopmenu['level'];
                            $this->response['topmenu'] = $topmenu;
                            $this->response['id'] = $id;
                        } else {
                            $this->errors = 'Ошибка: неизвестный идентификатор пункта меню';
                        }
                    }
                } else {
                    $this->errors='Ошибка: Пришла пустая форма с настройками';    
                }
            break;
            case "new_add":
                $topmenu = waRequest::post("topmenu", array());
                if ($topmenu && count($topmenu) > 0) {
                    $mTopmenu = new shopTopmenuTreeModel();
                    if ($topmenu['level'] == 2 || $topmenu['level'] == 1) {
                        if (isset($topmenu['hide'])) {
                            $topmenu['hide'] = 1;
                        } else {
                            $topmenu['hide'] = 0;
                        }
                        $topmenu_id = $mTopmenu->insert($topmenu);
                        if ($topmenu_id) {
                            $this->response['topmenu'] = $topmenu;    
                            $this->response['id'] = $topmenu_id;
                        } else {
                            $this->errors = 'Ошибка: не получается создать пункт меню';
                        }
                    }
                } else {
                    $this->errors='Ошибка: Пришла пустая форма с настройками';    
                }
            break;
            default:
                $this->errors='Ошибка: Неизвестная операция';
            break;
        }
    }
}