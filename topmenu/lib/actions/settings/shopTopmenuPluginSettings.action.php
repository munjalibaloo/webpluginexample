<?php

class shopTopmenuPluginSettingsAction extends waViewAction {
    
    public function execute() {
        $plugin = wa('shop')->getPlugin('topmenu');
        $aMenus = $this->getTreeMenusNew();
        if ($aMenus) {
            $this->view->assign("aMenus", $aMenus);
        }
    }

    function getTreeMenusNew() {
        $mTopmenu = new shopTopmenuTreeModel();
        $result = array();
        $aGrps = $mTopmenu->query("SELECT * FROM shop_topmenu_tree WHERE parent_id = 0 ORDER BY `sort_order`, name")->fetchAll();
        if ($aGrps) {
            foreach ($aGrps as $oGrp) {
                $result[$oGrp['id']] = $oGrp;
                $aSubGrps = $mTopmenu->query("SELECT * FROM shop_topmenu_tree WHERE parent_id = ".$oGrp['id']." && level = 1 ORDER BY `sort_order`, name")->fetchAll();
                if ($aSubGrps) {
                    foreach ($aSubGrps as &$oSubGrp) {
                        $aSubSub = $mTopmenu->query("SELECT * FROM shop_topmenu_tree WHERE parent_id = ".$oSubGrp['id']."  && level = 2 ORDER BY `sort_order`, name")->fetchAll();
                        if ($aSubSub) {
                            foreach ($aSubSub as $oSubSub) {
                                $oSubGrp['tree'][] = $oSubSub;
                            }
                        }
                        $result[$oGrp['id']]['tree'][$oSubGrp['id']] = $oSubGrp;
                    }
                    unset($oSubGrp);
                }
                $aSubBrands = $mTopmenu->query("SELECT * FROM shop_topmenu_tree WHERE parent_id = ".$oGrp['id']." && level = 4  ORDER BY name")->fetchAll();
                if ($aSubBrands) {
                    foreach ($aSubBrands as $oSubBrand) {
                        $result[$oGrp['id']]['brands'][$oSubBrand['id']] = $oSubBrand;
                    }
                }

            }
        }
        return $result;
    }
}
