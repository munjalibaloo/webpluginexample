<?php

class shopTopmenuPluginBackendFormAction extends waViewAction {
    
    public function execute() {
        $tps = array(
            "full" => "Ссылка на группу",
            "filt" => "Ссылка на фильтр",
            "link" => "Ссылка общая"
        );
        $plugin = wa()->getPlugin('topmenu');
        $id = waRequest::post('id', 0, "int");
        if ($id > 0) {
            $mTopmenu = new shopTopmenuTreeModel();
            $oTopmenu = $mTopmenu->getById($id);
            if ($oTopmenu) {
                $this->view->assign('oTopmenu', $oTopmenu);
                if ($oTopmenu['parent_id'] > 0) {
                    $oParentMenu = $mTopmenu->getById($oTopmenu['parent_id']);
                    $fnd_arr = array(
                        'parent_id' => $oParentMenu['parent_id'],
                        'level' => array(0, 1, 2)
                    );
                    
                    $aParents = $mTopmenu->getByField($fnd_arr, true);
                    $this->view->assign('aParents', $aParents);
                }
                if ($oTopmenu['categ_id'] > 0) {
                    if ($oTopmenu['tp'] == 'full') {
                        $mCategory = new shopCategoryModel();
                        $oCateg = $mCategory->getById($oTopmenu['categ_id']);
                        $this->view->assign('oCateg', $oCateg);    
                    }
                    
                }
            } else {
                $this->view->assign('error', "Ошибка: Неверный идентификатор пункта меню");    
            }
        } else {
            $parent_id = waRequest::post('parent', 0, "int");
            $level = waRequest::post('level', 0, "int");
            if ($parent_id > 0) {
                $mTopmenu = new shopTopmenuTreeModel();
                $oParent = $mTopmenu->getById($parent_id);
                if ($oParent) {
                    $topmenu_new = array(
                        'name' => "",
                        'url' => "",
                        "sort_order" => 0,
                        "hide" => 0,
                        "parent_id" => $parent_id,
                        "tp" => "link",
                        "categ_id" => 0
                    );

                    $this->view->assign("topmenu_new", $topmenu_new);
                    $this->view->assign("tps", $tps);
                    $this->view->assign("level", $level);
                    $this->view->assign('oParent', $oParent);
                } else {
                    $this->view->assign('error', "Ошибка: Неверный идентификатор родительского пункта меню");    
                }
            } else {
                $this->view->assign('error', "Ошибка: Неверный идентификатор родительского пункта меню");
            }
            
        }
    }
}