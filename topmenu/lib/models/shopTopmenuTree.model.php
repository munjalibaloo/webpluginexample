<?php
class shopTopmenuTreeModel extends waModel {

	protected $table = 'shop_topmenu_tree';

	function getFormatedTopmenu() {
        $aGrps = $this->query("SELECT * FROM {$this->table} WHERE parent_id = 0 ORDER BY sort_order, name")->fetchAll();
        if ($aGrps) {
            foreach ($aGrps as $ic => $oGrp) {
                $aSubs = $this->query("SELECT * FROM {$this->table} WHERE parent_id = ".$oGrp['id']." ORDER BY level, sort_order, name")->fetchAll();
                if ($aSubs) {
                    foreach ($aSubs as $oSub) {
                        $aGrps[$ic]['level_'.$oSub['level']][] = $oSub;
                    }
                }
            }
        }
        return $aGrps;
	}
}

?>